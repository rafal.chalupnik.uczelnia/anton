﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Implementation.Model;

namespace Implementation
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var guid = Request.QueryString["activationid"];
            var result = Logic.ActivateReviewer(guid);

            PassResult.Visible = result;
            FailResult.Visible = !result;
        }
    }
}