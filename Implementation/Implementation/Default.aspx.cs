﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Implementation.Model;
using Implementation.Model.Dao;
using Implementation.Model.Dto;

namespace Implementation
{
    public partial class _Default : Page
    {
        private void MessageBox(string message)
        {
            ScriptManager.RegisterStartupScript(this, GetType(),
                "ServerControlScript", $"alert(\"{message}\");", true);
        }
        
        public List<Expert> ExpertList { get; } = new List<Expert>();
        public List<Reviewer> ReviewerList { get; } = new List<Reviewer>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Logic.StartLogic();

            ExpertView.DataSource = Logic.GetExperts();
            ExpertView.DataBind();

            PendingView.DataSource = Logic.GetPending();
            PendingView.DataBind();

            ReviewerView.DataSource = Logic.GetReviewers();
            ReviewerView.DataBind();
        }

        protected void AddExpertOnClick(object sender, EventArgs e)
        {
            var result = Logic.AddExpert(NewExpertName.Text, NewExpertSurname.Text, NewExpertEmail.Text);
            if (result)
                Response.Redirect(Request.RawUrl);
            else
                MessageBox("Dodawanie eksperta nie powiodło się. Sprawdź poprawność danych.");
        }

        protected void SendEmailOnClick(object sender, EventArgs e)
        {
            var button = sender as Button;
            Logic.CreateActivationRequest(button.CommandArgument);
            Response.Redirect(Request.RawUrl);
        }
    }
}