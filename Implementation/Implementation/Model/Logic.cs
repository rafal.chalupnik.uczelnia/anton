﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Implementation.Model.Dao;
using Implementation.Model.Dto;

namespace Implementation.Model
{
    public static class Logic
    {
        private static EmailSender emailSender = 
            new EmailSender("smtp.gmail.com", 587, "rafal.chalupnik7@gmail.com", "password", true);
        private static List<ActivationRequest> activationRequests =
            new List<ActivationRequest>();
        private static Random random = new Random();

        public static void StartLogic()
        {
            BaseDao.CreateDatabaseIfNecessary();
            activationRequests = ActivationRequestDao.Select();
        }

        public static bool AddExpert(string name, string surname, string email)
        {
            if (string.IsNullOrEmpty(name))
                return false;
            if (string.IsNullOrEmpty(surname))
                return false;
            if (string.IsNullOrEmpty(email))
                return false;

            if (ExpertDao.Select().FirstOrDefault(e => e.Email.Equals(email)) != null)
                return false;

            ExpertDao.Insert(new Expert()
            {
                Email = email,
                Name = name,
                Surname = surname
            });

            return true;
        }
        public static bool ActivateReviewer(string guidString)
        {
            try
            {
                var guid = Guid.Parse(guidString);
                var request = activationRequests.FirstOrDefault(r => r.Guid.Equals(guid));
                if (request == null) return false;

                var expert = ExpertDao.Select().First(e => e.Email.Equals(request.Email));
                var reviewer = new Reviewer()
                {
                    Active = true,
                    Email = expert.Email,
                    Login = $"{expert.Name}_{expert.Surname}_{Guid.NewGuid().ToString("N")}",
                    Password = RandomString(10),
                    Name = expert.Name,
                    Surname = expert.Surname
                };
                ReviewerDao.Insert(reviewer);
                ExpertDao.Delete(expert);
                activationRequests.Remove(request);
                ActivationRequestDao.Delete(request);

                SendWelcomeEmail(reviewer);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
        public static void CreateActivationRequest(string emailAddress)
        {
            var expert = ExpertDao.Select().First(e => e.Email.Equals(emailAddress));
            var request = new ActivationRequest()
            {
                Email = emailAddress,
                Guid = Guid.NewGuid()
            };
            activationRequests.Add(request);
            ActivationRequestDao.Insert(request);

            SendInvitationEmail(expert, request.Guid);
            expert.EmailSent = true;
            ExpertDao.Update(expert);
        }
        public static List<Expert> GetExperts()
        {
            return ExpertDao.Select().Where(ex => !ex.EmailSent).ToList();
        }
        public static List<Expert> GetPending()
        {
            return ExpertDao.Select().Where(ex => ex.EmailSent).ToList();
        }
        public static List<Reviewer> GetReviewers()
        {
            return ReviewerDao.Select();
        }
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static void SendInvitationEmail(Expert expert, Guid activationGuid)
        {
            var url = HttpContext.Current.Request.Url;
            var link = $"http://{url.Host}:{url.Port}/Activate?activationid={activationGuid}";
            emailSender.SendEmail(expert.Email, "Zaproszenie", $"Guid aktywacji: {link}");
        }
        public static void SendWelcomeEmail(Reviewer reviewer)
        {
            var content = "Witamy w konkursie ABC. Poniżej przesyłamy dane do logowania:\n";
            content += $"Login: {reviewer.Login}\n";
            content += $"Hasło: {reviewer.Password}";
            emailSender.SendEmail(reviewer.Email, "Dane logowania", content);
        }
    }
}