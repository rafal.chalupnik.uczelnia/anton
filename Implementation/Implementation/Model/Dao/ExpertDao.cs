﻿using System.Collections.Generic;
using System.Linq;
using Implementation.Model.Dto;

namespace Implementation.Model.Dao
{
    public class ExpertDao : BaseDao
    {
        public const string DeleteSql = "DELETE FROM Expert WHERE Email = @Email";
        public const string InsertSql = "INSERT INTO Expert(Email, Name, Surname, EmailSent) VALUES (@Email, @Name, @Surname, @EmailSent)";
        public const string SelectSql = "SELECT * FROM Expert";
        public const string UpdateSql =
            "UPDATE Expert SET Name = @Name, Surname = @Surname, EmailSent = @EmailSent WHERE Email = @Email";

        public static void Delete(Expert expert)
        {
            SqlExecute(DeleteSql, expert);
        }
        public static void Insert(Expert expert)
        {
            SqlExecute(InsertSql, expert);
        }
        public static List<Expert> Select()
        {
            return SqlQuery<Expert>(SelectSql).ToList();
        }

        public static void Update(Expert expert)
        {
            SqlExecute(UpdateSql, expert);
        }
    }
}