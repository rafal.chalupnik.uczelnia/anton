﻿using System.Collections.Generic;
using System.Linq;
using Implementation.Model.Dto;

namespace Implementation.Model.Dao
{
    public class ReviewerDao : BaseDao
    {
        public const string InsertSql =
                "INSERT INTO Reviewer(Email, Name, Surname, Login, Password, Active) VALUES (@Email, @Name, @Surname, @Login, @Password, @Active)";
        public const string SelectSql =
            "SELECT * FROM Reviewer";

        public static void Insert(Reviewer reviewer)
        {
            SqlExecute(InsertSql, reviewer);
        }
        public static List<Reviewer> Select()
        {
            return SqlQuery<Reviewer>(SelectSql).ToList();
        }
    }
}