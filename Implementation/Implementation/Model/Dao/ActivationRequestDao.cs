﻿using System.Collections.Generic;
using System.Linq;

namespace Implementation.Model.Dao
{
    public class ActivationRequestDao : BaseDao
    {
        public const string DeleteSql = "DELETE FROM ActivationRequest WHERE Email = @Email AND Guid = @Guid";
        public const string InsertSql = "INSERT INTO ActivationRequest(Email, Guid) VALUES (@Email, @Guid)";
        public const string SelectSql = "SELECT * FROM ActivationRequest";

        public static void Delete(ActivationRequest request)
        {
            SqlExecute(DeleteSql, request);
        }
        public static void Insert(ActivationRequest request)
        {
            SqlExecute(InsertSql, request);
        }
        public static List<ActivationRequest> Select()
        {
            return SqlQuery<ActivationRequest>(SelectSql).ToList();
        }
    }
}