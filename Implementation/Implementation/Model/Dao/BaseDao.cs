﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Web;
using Dapper;

namespace Implementation.Model
{
    public class BaseDao
    {
        private const string CreateActivationRequestTableSql =
                "CREATE TABLE ActivationRequest(Email NVARCHAR(50) NOT NULL, Guid uniqueidentifier NOT NULL, PRIMARY KEY(Email, Guid))";
        private const string CreateExpertTableSql =
                "CREATE TABLE Expert(Email NVARCHAR(50) NOT NULL PRIMARY KEY, Name NVARCHAR(20) NOT NULL, Surname NVARCHAR(30) NOT NULL, EmailSent BIT NOT NULL)";
        private const string CreateReviewerTableSql =
                "CREATE TABLE Reviewer(Login NVARCHAR(50) NOT NULL PRIMARY KEY, Email NVARCHAR(50) NOT NULL UNIQUE,  Name NVARCHAR(20) NOT NULL,  Surname NVARCHAR(30) NOT NULL, Password NVARCHAR(50) NOT NULL, Active BIT NOT NULL)";

        protected static string ConnectionString => $"Data Source={DatabasePath};Version=3;";

        protected static string DatabasePath { get; } =
            Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "Database.db");

        protected static void SqlExecute(string sql, object @params = null)
        {
            using (var connection = new SQLiteConnection(ConnectionString))
            {
                connection.Execute(sql, @params);
            }
        }
        protected static IEnumerable<T> SqlQuery<T>(string sql, object @params = null)
        {
            using (var connection = new SQLiteConnection(ConnectionString))
            {
                return connection.Query<T>(sql, @params);
            }
        }

        public static void CreateDatabaseIfNecessary()
        {
            if (File.Exists(DatabasePath))
                return;

            SqlExecute(CreateActivationRequestTableSql);
            SqlExecute(CreateExpertTableSql);
            SqlExecute(CreateReviewerTableSql);
        }
    }
}