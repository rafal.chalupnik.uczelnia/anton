﻿using System;

namespace Implementation.Model.Dao
{
    public class ActivationRequest
    {
        public string Email { get; set; }
        public Guid Guid { get; set; }
    }
}