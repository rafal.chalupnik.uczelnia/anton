﻿namespace Implementation.Model.Dto
{
    public class Expert
    {
        public string Email { get; set; }
        public bool EmailSent { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}