﻿namespace Implementation.Model.Dto
{
    public class Reviewer : Expert
    {
        public bool Active { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}