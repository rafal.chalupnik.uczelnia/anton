﻿<%@ Page Title="Home Page" EnableEventValidation="false" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Implementation._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Zarządzanie ekspertami i recenzentami</h1>

<div class="row">
    <div class="col-md-4">
        <table>
            <tr>
                <td>
                    <h2>Eksperci</h2>
                </td>
                <td>
                    <button type="button" data-target="#popup" data-toggle="modal" class="btn btn-primary btn-xs">Dodaj eksperta</button>
                </td>
            </tr>
        </table>
        <table class="table">
            <thead style="font-weight: bold">
            <tr>
                <td>Imię</td>
                <td>Nazwisko</td>
                <td>Email</td>
                <td>Wyślij e-mail</td>
            </tr>
            </thead>
            <asp:Repeater ID="ExpertView" runat="server">
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("Name") %></td>
                        <td><%# Eval("Surname") %></td>
                        <td><%# Eval("Email") %></td>
                        <td>
                            <asp:Button runat="server" Text="Wyślij"
                                        OnClientClick="return confirm('Czy na pewno chcesz wysłać email?);"
                                        CommandArgument='<%#Eval("Email")%>' CommandName="SendEmail"
                                        OnClick="SendEmailOnClick"/>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

    </div>
    <div class="col-md-4">
        <h2>Oczekujące</h2>
        <table class="table">
            <thead style="font-weight: bold">
            <tr>
                <td>Imię</td>
                <td>Nazwisko</td>
                <td>Email</td>
            </tr>
            </thead>
            <asp:Repeater ID="PendingView" runat="server">
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("Name") %></td>
                        <td><%# Eval("Surname") %></td>
                        <td><%# Eval("Email") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

    </div>
</div>
    <div class="row">
        <h2>Recenzenci</h2>
        <table class="table">
            <thead style="font-weight: bold">
            <tr>
                <td>Imię</td>
                <td>Nazwisko</td>
                <td>Email</td>
                <td>Login</td>
                <td>Hasło</td>
                <td>Aktywny</td>
            </tr>
            </thead>
            <asp:Repeater ID="ReviewerView" runat="server">
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("Name") %></td>
                        <td><%# Eval("Surname") %></td>
                        <td><%# Eval("Email") %></td>
                        <td><%# Eval("Login") %></td>
                        <td><%# Eval("Password") %></td>
                        <td>
                            <asp:CheckBox runat="server" Checked='<%#Eval("Active")%>' Enabled="False"/>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <div id="popup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Dodaj eksperta</h4>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>Imię:</td>
                            <td>
                                <asp:TextBox runat="server" ID="NewExpertName"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Nazwisko:</td>
                            <td>
                                <asp:TextBox runat="server" ID="NewExpertSurname"/>
                            </td>
                        </tr>
                        <tr>
                            <td>E-mail:</td>
                            <td>
                                <asp:TextBox runat="server" ID="NewExpertEmail"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                    <asp:Button runat="server" Text="Dodaj" CssClass="btn btn-primary" OnClick="AddExpertOnClick"/>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
