﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Activate.aspx.cs" Inherits="Implementation.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="PassResult" runat="server">
        <h1>Twoje konto zostało aktywowane!</h1>
        <p>Tutaj dodatkowa treść :)</p>
    </div>
    <div id="FailResult" runat="server">
        <h1>Nieprawidłowy token aktywacji.</h1>
        <p>Konto, które próbujesz aktywować, nie istnieje.</p>
    </div>
</asp:Content>
